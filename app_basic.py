"""
Basic app

:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2018-09-02
"""

import argparse

from taxi_bot.bots import base_bot
from taxi_bot.services import taxi_service
from taxi_bot.repositories.rest import taxi_repository


def setup(address, port):
    taxi_repo = taxi_repository.TaxiRepository()
    taxi_repo.setup(address, port)
    taxi_s = taxi_service.TaxiService(taxi_repo)
    taxi_b = base_bot.BaseBot(taxi_s)
    return taxi_b

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--address', default='35.204.38.8', action='store')
    parser.add_argument('--port', default=4000, action='store')
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = parse_arguments()
    bot = setup(args.address, args.port)
    print 'Welcome to the taxi service bot!'
    print 'Use "help" to see the available services'
    print 'Use "exit" to finish with the service'
    while True:
        print 'Waiting for your command sir:\n'
        command = raw_input()
        if command == 'exit':
            print 'Good bye!'
            break
        elif command == 'help':
            print bot.help()
        else:
            print bot.process_command(command)
