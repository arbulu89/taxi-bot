"""
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2018-09-02
"""

from taxi_bot.repositories.rest import taxi_repository

def before_all(context):
    repo = taxi_repository.TaxiRepository()
    context.taxi_repo = repo
