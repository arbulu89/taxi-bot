"""
When steps
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2018-09-02
"""

from behave import given

@given(u'the taxi service is exposed in {address} and {port} port')
def step_impl(context, address, port):
    context.taxi_repo.setup(address, port)
