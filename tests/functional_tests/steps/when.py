"""
When steps
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2018-09-02
"""

from taxi_bot.repositories import base_repository

from behave import when


@when(u'all taxis information is requested')
def step_impl(context):
    context.all_taxis = context.taxi_repo.get_taxis()

@when(u'available taxis in {city} are requested')
def step_impl(context, city):
    context.available_taxis = context.taxi_repo.get_taxis_by_city(city)

@when(u'available taxis in {city} are requested until {count:d} is available')
def step_impl(context, city, count):
    while True:
        context.available_taxis = context.taxi_repo.get_taxis_by_city(city)
        if len(context.available_taxis) >= count:
            break

@when(u'the first taxi information of {city} is requested')
def step_impl(context, city):
    for taxi in context.all_taxis:
        if taxi['city'] == city:
            taxi_id = taxi['name']
            break
    context.taxi_info = context.taxi_repo.get_taxi_info(city, taxi_id)

@when(u'the same taxi information of {city} is requested')
def step_impl(context, city):
    taxi_id = context.hired_taxi_info['name']
    context.hired_taxi_info = context.taxi_repo.get_taxi_info(city, taxi_id)

@when(u'the first taxi information is requested using other city')
def step_impl(context):
    taxi_id = context.available_taxis[0]['name']
    context.taxi_info = context.taxi_repo.get_taxi_info('unknown', taxi_id)

@when(u'the first available taxi in {city} is hired')
def step_impl(context, city):
    for taxi in context.available_taxis:
        if taxi['city'] == city:
            taxi_id = taxi['name']
            break
    try:
        context.hired_taxi_info = context.taxi_repo.hire_taxi(city, taxi_id)
    except base_repository.NotAvailableTaxiError:
        context.taxi_not_available = True

@when(u'the first taxi is hired using the other city')
def step_impl(context):
    taxi_id = context.available_taxis[0]['name']
    try:
        context.hired_taxi_info = context.taxi_repo.hire_taxi('unknown', taxi_id)
    except base_repository.NotAvailableTaxiError:
        context.taxi_not_available = True

@when(u'the same taxi is hired')
def step_impl(context):
    city = context.hired_taxi_info['city']
    taxi_id = context.hired_taxi_info['name']
    try:
        context.hired_taxi_info = context.taxi_repo.hire_taxi(city, taxi_id)
    except base_repository.NotAvailableTaxiError:
        context.taxi_not_available = True
