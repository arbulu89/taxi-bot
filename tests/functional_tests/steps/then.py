"""
Then steps
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2018-09-02
"""

import time

from taxi_bot.repositories import base_repository

from behave import then

@then(u'retrieved taxis information is correct')
def step_imple(context):
    for taxi in context.all_taxis:
        assert('name' in taxi)
        assert('location' in taxi)
        assert('state' in taxi)
        assert('city' in taxi)

@then(u'only information from the taxis of {city} is retrieved')
def step_impl(context, city):
    for taxi in context.available_taxis:
        assert(taxi['city'] == city)

@then(u'empty list is retrieved')
def step_imple(context):
    assert(not context.available_taxis)

@then(u'retrieved taxi information is correct')
def step_impl(context):
    assert('name' in context.taxi_info)
    assert('location' in context.taxi_info)
    assert('state' in context.taxi_info)
    assert('city' in context.taxi_info)

@then(u'retrieved empty information is retrieved')
def step_imple(context):
    assert(not context.taxi_info), context.taxi_info

@then(u'taxi state is hired')
@then(u'the taxi hiring process is correctly done')
def step_impl(context):
    assert('hired' == context.hired_taxi_info['state'])

@then(u'the taxi hiring process returns an error response')
def step_impl(context):
    assert(hasattr(context, 'taxi_not_available') == True), context.hired_taxi_info

@then(u'a not available response is retrieved')
def step_impl(context):
    assert(hasattr(context, 'taxi_not_available') == True)

@then(u'last taxi is not available')
def step_impl(context):
    for taxi in context.available_taxis:
        assert(taxi['name'] != context.hired_taxi_info['name'])

@then(u'last taxi is in the list')
def step_impl(context):
    for taxi in context.all_taxis:
        if(taxi['name'] == context.hired_taxi_info['name']):
            break
    else:
        assert(False), context.all_taxis

@then(u'the same taxi is available again before {sec:d} seconds')
def step_impl(context, sec):
    current_time = time.time()
    timeout = current_time + sec
    city = context.hired_taxi_info['city']
    taxi_id = context.hired_taxi_info['name']
    while current_time < timeout:
        try:
            context.hired_taxi_info = context.taxi_repo.hire_taxi(city, taxi_id)
            break
        except base_repository.NotAvailableTaxiError:
            time.sleep(0.5)
    else:
        assert(False), 'taxi was not released before {} seconds'.format(sec)
