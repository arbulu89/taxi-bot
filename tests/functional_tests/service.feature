Feature: Test taxi hiring service

    Background: Taxi service connection is done
        Given the taxi service is exposed in 35.204.38.8 and 4000 port

    Scenario: When all taxis are requested Then taxis information is retrieved
        When all taxis information is requested
        Then retrieved taxis information is correct

    Scenario Outline: When some city taxis are requested Then only this city taxis are retrieved
        When available taxis in <city> are requested
        Then only information from the taxis of <city> is retrieved

        Examples: Cities
        |city|
        |Madrid|
        |Barcelona|

    Scenario: When unknown city taxis are requested Then empty list is retrieved
        When available taxis in London are requested
        Then empty list is retrieved

    Scenario: When some taxi information is requested Then correct information is retrieved
        When all taxis information is requested
        And the first taxi information of Madrid is requested
        Then retrieved taxi information is correct

    Scenario: When some taxi information in an incorrect city is requested Then an empty response is retrieved
        When available taxis in Madrid are requested until 1 is available
        And the first taxi information is requested using other city
        Then retrieved empty information is retrieved

    Scenario: When some taxi is hired Then hiring process is correctly processed
        When available taxis in Madrid are requested until 1 is available
        And the first available taxi in Madrid is hired
        Then the taxi hiring process is correctly done

    Scenario: When some taxi is hired Then the taxi information says that is hired
        When available taxis in Madrid are requested until 1 is available
        And the first available taxi in Madrid is hired
        Then the taxi hiring process is correctly done

        When the same taxi information of Madrid is requested
        Then taxi state is hired

    Scenario: When some taxi is hired Then hiring process is correctly processed
        When available taxis in Madrid are requested until 1 is available
        And the first taxi is hired using the other city
        Then the taxi hiring process returns an error response

    Scenario: When multiple taxis are hired Then hiring process is correctly processed
        When available taxis in Madrid are requested until 2 is available
        And the first available taxi in Madrid is hired
        Then the taxi hiring process is correctly done

        When available taxis in Madrid are requested until 1 is available
        And the first available taxi in Madrid is hired
        Then the taxi hiring process is correctly done

    Scenario: When some taxi is hired and other hiring request arrives Then hiring taxi not available response is retrieved
        When available taxis in Madrid are requested until 1 is available
        And the first available taxi in Madrid is hired
        Then the taxi hiring process is correctly done

        When the same taxi is hired
        Then a not available response is retrieved

        When the same taxi is hired
        Then a not available response is retrieved

    Scenario: When some taxi is hired Then is released again before 10 seconds
        When available taxis in Madrid are requested until 1 is available
        And the first available taxi in Madrid is hired
        Then the taxi hiring process is correctly done
        And the same taxi is available again before 10 seconds
        And the taxi hiring process is correctly done

    Scenario: When some taxi is hired Then is not available at this moment
        When available taxis in Madrid are requested until 1 is available
        And the first available taxi in Madrid is hired
        Then the taxi hiring process is correctly done

        When available taxis in Madrid are requested
        Then last taxi is not available

    Scenario: When some taxi is hired Then the taxi is still retrieved in all taxis request
        When available taxis in Madrid are requested until 1 is available
        And the first available taxi in Madrid is hired
        Then the taxi hiring process is correctly done

        When all taxis information is requested
        Then last taxi is in the list
