"""
Taxi service unit tests
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2018-09-02
"""

# pylint:disable=C0103,C0111,W0212,W0611

import logging
import unittest

import mock

from taxi_bot.services.utils import coordinates
from taxi_bot.services import taxi_service
from taxi_bot.repositories import base_repository


class TestTaxiService(unittest.TestCase):
    """
    Unitary tests for TaxiService.
    """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

        logging.basicConfig(level=logging.INFO)

    def setUp(self):
        """
        Test setUp.
        """
        self._mock_repo = mock.Mock()
        self._service = taxi_service.TaxiService(self._mock_repo)

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

    def test_get_taxi_info(self):
        self._mock_repo.get_taxi_info.return_value = 'My taxi'
        taxis = self._service.get_taxi_info('London', 'Tesla')
        self.assertEqual('My taxi', taxis)

    def test_get_taxi_info_error(self):
        self._mock_repo.get_taxi_info.return_value = []
        with self.assertRaises(taxi_service.TaxiNotExistError) as err:
            self._service.get_taxi_info('London', 'Tesla')
        self.assertTrue(
            'taxi with this name does not exist in {}'.format('London') in err.exception)

    @mock.patch('taxi_bot.services.utils.coordinates.get_distance')
    def test_get_taxi_distances(self, mock_get_distance):
        self._service.get_taxis_by_city = mock.Mock()
        self._service.get_taxis_by_city.return_value = [
            {'location': {'lat': 1, 'lon': 2}},
            {'location': {'lat': 2, 'lon': 1}}
        ]
        mock_get_distance.side_effect = [10, 20]
        taxis = self._service._get_taxi_distances('London', 1, 3)

        self._service.get_taxis_by_city.assert_called_once_with('London')
        mock_get_distance.assert_has_calls([
            mock.call(1, 3, 1, 2),
            mock.call(1, 3, 2, 1)
        ])

        self.assertEqual(
            [taxi_service.Taxi(taxi_info={'location': {'lat': 1, 'lon': 2}}, distance=10),
            taxi_service.Taxi(taxi_info={'location': {'lat': 2, 'lon': 1}}, distance=20)],
            taxis
        )

    def test_get_taxi_distances_emptry(self):
        self._service.get_taxis_by_city = mock.Mock()
        self._service.get_taxis_by_city.return_value = []
        taxis = self._service._get_taxi_distances('London', 1, 3)

        self._service.get_taxis_by_city.assert_called_once_with('London')

        self.assertEqual(
            [],
            taxis
        )

    def test_hire_nearest_taxi(self):

        self._service._get_taxi_distances = mock.Mock()
        self._service.hire_taxi = mock.Mock()

        self._service._get_taxi_distances.return_value = [
            taxi_service.Taxi(taxi_info={'name': 'A', 'location': {'lat': 1, 'lon': 2}}, distance=49),
            taxi_service.Taxi(taxi_info={'name': 'B', 'location': {'lat': 3, 'lon': 4}}, distance=10),
            taxi_service.Taxi(taxi_info={'name': 'C', 'location': {'lat': 5, 'lon': 6}}, distance=20),
            taxi_service.Taxi(taxi_info={'name': 'D', 'location': {'lat': 7, 'lon': 8}}, distance=4)]

        self._service.hire_taxi.side_effect = [
            base_repository.NotAvailableTaxiError,
            base_repository.NotAvailableTaxiError,
            {'name': 'C', 'location': {'lat': 5, 'lon': 6}}
        ]

        taxi = self._service.hire_nearest_taxi('London', 1, 2)

        self._service.hire_taxi.assert_has_calls([
            mock.call('London', 'D'),
            mock.call('London', 'B'),
            mock.call('London', 'C')
        ])

        self.assertEqual({'name': 'C', 'location': {'lat': 5, 'lon': 6}}, taxi)

    def test_hire_nearest_taxi_not_available(self):
        self._service._get_taxi_distances = mock.Mock()
        self._service._get_taxi_distances.return_value = []

        with self.assertRaises(taxi_service.NotAvailableTaxiError) as err:
            self._service.hire_nearest_taxi('London', 1, 2)
        self.assertTrue('There is not any taxi available at this moment in {}'.format('London') in err.exception)

    def test_hire_nearest_taxi_not_available_2(self):

        self._service._get_taxi_distances = mock.Mock()
        self._service.hire_taxi = mock.Mock()

        self._service._get_taxi_distances.return_value = [
            taxi_service.Taxi(taxi_info={'name': 'A', 'location': {'lat': 1, 'lon': 2}}, distance=49),
            taxi_service.Taxi(taxi_info={'name': 'B', 'location': {'lat': 3, 'lon': 4}}, distance=10),
            taxi_service.Taxi(taxi_info={'name': 'C', 'location': {'lat': 5, 'lon': 6}}, distance=20),
            taxi_service.Taxi(taxi_info={'name': 'D', 'location': {'lat': 7, 'lon': 8}}, distance=4)]

        self._service.hire_taxi.side_effect = [
            base_repository.NotAvailableTaxiError,
            base_repository.NotAvailableTaxiError,
            base_repository.NotAvailableTaxiError,
            base_repository.NotAvailableTaxiError
        ]

        with self.assertRaises(taxi_service.NotAvailableTaxiError) as err:
            self._service.hire_nearest_taxi('London', 1, 2)
        self.assertTrue('There is not any taxi available at this moment in {}'.format('London') in err.exception)

    def test_get_distance(self):
        distance = coordinates.get_distance(40.51, 45.92, 41.73, 42.84)
        self.assertEqual(292, round(distance))

        distance = coordinates.get_distance(-40.50, 45.90, 41.70, -42.80)
        self.assertEqual(12767, round(distance))
