"""
Taxi service
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2018-09-02
"""

import logging
import collections
import operator

from taxi_bot.services.utils import coordinates
from taxi_bot.repositories import base_repository

Taxi = collections.namedtuple('Taxi', ['taxi_info', 'distance'])


class BaseError(Exception):
    """
    Base error
    """


class NotAvailableTaxiError(BaseError):
    """
    No taxi available
    """

class TaxiNotExistError(BaseError):
    """
    Taxi does not exist
    """


class TaxiService(object):
    """
    Taxi service

    Args:
        repository (base_repository): Used repository instance
    """

    def __init__(self, repository):
        self._logger = logging.getLogger(__name__)
        self._repository = repository

    def get_taxis(self):
        """
        Get taxis
        """
        return self._repository.get_taxis()

    def get_taxis_by_city(self, city):
        """
        Hire taxi

        Args:
            city (str): City to get the taxi information
        """
        return self._repository.get_taxis_by_city(city)

    def get_taxi_info(self, city, taxi_id):
        """
        Hire taxi

        Args:
            city (str): City to get the taxi information
            taxi_id (str): Taxi name
        """
        taxi_info = self._repository.get_taxi_info(city, taxi_id)
        if not taxi_info:
            raise TaxiNotExistError(
                'taxi with this name does not exist in {}'.format(city))
        return taxi_info

    def hire_taxi(self, city, taxi_id):
        """
        Hire taxi

        Args:
            city (str): City to get the taxi information
            taxi_id (str): Taxi name
        """
        return self._repository.hire_taxi(city, taxi_id)

    def _get_taxi_distances(self, city, lat, lon):
        """
        Get taxi distances. Only available taxis

        Returns:
            list: List with TAXI type elements
        """
        taxi_list = []
        for taxi in self.get_taxis_by_city(city):
            distance = coordinates.get_distance(
                lat, lon, taxi['location']['lat'], taxi['location']['lon'])
            taxi_list.append(Taxi(taxi_info=taxi, distance=distance))
        return taxi_list

    def hire_nearest_taxi(self, city, lat, lon):
        """
        Hire nearest available taxi providing some coordinates

        Args:
            city (str): City to hire the taxi
            lat (float): Latitude in degrees
            lon (float): Longiture in degrees

        Raises:
            NotAvailableTaxiError: When no taxi is available
        """

        taxi_list = self._get_taxi_distances(city, lat, lon)
        for taxi in sorted(taxi_list, key=operator.attrgetter('distance')):
            try:
                response = self.hire_taxi(city, taxi.taxi_info['name'])
                break
            except base_repository.NotAvailableTaxiError:
                continue
        else:
            raise NotAvailableTaxiError(
                'There is not any taxi available at this moment in {}'.format(
                    city))

        self._logger.info(
            'taxi with id %s hired in location (lat,lon): %f,%f',
            response['name'],
            response['location']['lat'],
            response['location']['lon'])
        return response
