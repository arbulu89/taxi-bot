"""
Taxi repository
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2018-09-02
"""

import logging
import requests

from taxi_bot.repositories import base_repository


class TaxiRepository(base_repository.BaseRepository):
    """
    Taxi rest repository. Used to retrieve taxi service status using http rest interface

    GET /taxis -> Returns all the taxis in the system
    GET /taxis/<city>/ -> Returns all the free taxis in the city
    GET /taxis/<city>/<taxi_id> -> Returns the information of a taxi

    POST /taxis/<city>/<taxi_id>
        {state: hired}
    """

    BASE_RESOURCE = '/api/v1'

    GET_TAXIS = '/taxis'
    GET_TAXIS_CITY = '/taxis/{city}'
    GET_TAXI_INFO = '/taxis/{city}/{id}'
    HIRE_TAXI = '/taxis/{city}/{id}'

    HIRE_TAXI_DATA = {'state': 'hired'}
    TAXI_NOT_AVAILABLE_STATUS = 403
    HIRED = 'hired'

    def __init__(self):
        self._logger = logging.getLogger(__name__)
        self._url = None
        self._base_resource = None
        self._hire_taxi_data = None

    def setup(self, address, port):
        """
        Setup service address

        Args:
            address (str): Service address
            port (str/int): Service port
        """
        self._url = '{}:{}'.format(address, port)

    @property
    def base_resource(self, resource):
        """
        Update base resource (by default, '/api/v1/')

        Args:
            resource (str): New base resource
        """
        if not startswith(resource, '/'):
            raise ValueError('resource must start with /')
        self._base_resource = resource

    @property
    def hire_taxi_data(self, data):
        """
        Update hire taxi post message (by default, HIRE_TAXI_DATA')

        Args:
            resource (str): New base resource
        """
        if not isinstance(data, dict):
            raise ValueError('data must be dict type')
        self._hire_taxi_data = data

    def _create_base_url(self):
        resource = self._base_resource or self.BASE_RESOURCE
        return 'http://{}{}'.format(self._url, resource)

    def _get_hire_taxi_data(self):
        return self._hire_taxi_data or self.HIRE_TAXI_DATA

    def get_taxis(self):
        """
        Get taxis

        Returns:
            list: Taxis in the system
        """
        url = self._create_base_url()
        url = '{}{}'.format(url, self.GET_TAXIS)
        return requests.get(url).json()

    def get_taxis_by_city(self, city):
        """
        Get taxis by city

        Args:
            city (str): City to get the taxis
        Returns:
            list: Available taxis in the city
        """
        resource = self.GET_TAXIS_CITY.format(city=city)
        url = self._create_base_url()
        url = '{}{}'.format(url, resource)
        return requests.get(url).json()

    def get_taxi_info(self, city, taxi_id):
        """
        Get taxi information

        Args:
            city (str): City to get the taxi information
            taxi_id (str): Taxi name
        Returns:
            dict: Taxi info
        """
        resource = self.GET_TAXI_INFO.format(city=city, id=taxi_id)
        url = self._create_base_url()
        url = '{}{}'.format(url, resource)
        return requests.get(url).json()

    def hire_taxi(self, city, taxi_id):
        """
        Hire taxi

        Args:
            city (str): City to get the taxi information
            taxi_id (str): Taxi name
        Returns:
            dict: Hire taxi info
        """
        resource = self.HIRE_TAXI.format(city=city, id=taxi_id)
        url = self._create_base_url()
        url = '{}{}'.format(url, resource)
        hire_taxi_data = self._get_hire_taxi_data()
        response = requests.post(url, json=hire_taxi_data)
        if response.status_code == self.TAXI_NOT_AVAILABLE_STATUS:
            raise base_repository.NotAvailableTaxiError(
                'taxi not available in this moment')
        elif response.json()['state'] != self.HIRED:
            raise base_repository.HiringError(
                'error hiring the taxi')
        return response.json()
