"""
Base repository
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2018-09-02
"""

class BaseError(Exception):
    """
    Base error
    """

class NotAvailableTaxiError(BaseError):
    """
    No taxi available
    """

class HiringError(BaseError):
    """
    Hiring error
    """

class BaseRepository(object):
    """
    Base repository
    """

    def get_taxis(self):
        """
        Get taxis

        Returns:
            Requests.Response: Requests response
        """
        raise NotImplementedError

    def get_taxis_by_city(self, city):
        """
        Get taxis by city

        Args:
            city (str): City to get the taxis
        Returns:
            Requests.Response: Requests response
        """
        raise NotImplementedError

    def get_taxi_info(self, city, taxi_id):
        """
        Get taxi information

        Args:
            city (str): City to get the taxi information
            taxi_id (str): Taxi name
        Returns:
            Requests.Response: Requests response
        """
        raise NotImplementedError

    def hire_taxi(self, city, taxi_id):
        """
        Hire taxi

        Args:
            city (str): City to get the taxi information
            taxi_id (str): Taxi name
        Returns:
            Requests.Response: Requests response
        """
        raise NotImplementedError
