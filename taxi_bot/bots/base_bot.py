"""
Base bot
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2018-09-02
"""

import logging
import pprint
import re

from taxi_bot.services import taxi_service

class BaseBot(object):
    """
    Base bot implementation. This object implements all the used commands and the
    basic functionality
    """

    BASE_ANSWER = 'Here the answer:\n{}\nAlways at your disposal!'
    INVALID_ANSWER = 'I don\'t recognize this command! Check the options in README.md file\n'

    COMMANDS = {
        'ALL_TAXIS': re.compile('^all taxis$'),
        'ALL_TAXIS_CITY': re.compile('^all taxis in (\w+)$'),
        'TAXI_INFO': re.compile('^taxi info (\w+) in (\w+)$$'),
        'HIRE': re.compile('^hire (\w+) taxi in (\w+)$'),
        'HIRE_NEAREST': re.compile('^hire nearest in (\w+) (\w+) (\w+)$')
    }

    TAXIS_NOT_AVAILABLE_ANSWER = 'I\'m sorry, there are not available taxis right now in {}'
    TAXI_INFO_NOT_AVAILABLE_ANSWER = 'I\'m sorry, i don\'t have taxi {} information'


    def __init__(self, service):
        self._logger = logging.getLogger(__name__)
        self._service = service

    def _parse_command(self, command):
        for key, value in self.COMMANDS.iteritems():
            if value.match(command):
                return (key, value.match(command))
        else:
            return ('INVALID', None)

    def help(self):
        """
        Help command
        """
        text = ''
        for key, value in self.COMMANDS.iteritems():
            text += 'Name: {}. Command expression: {}\n'.format(key, value.pattern)
        return text

    def process_command(self, command):
        """
        Process command

        Args:
            command (str): command
        """
        command_type, reg = self._parse_command(command)
        if command_type == 'ALL_TAXIS':
            return self.get_taxis()
        elif command_type == 'ALL_TAXIS_CITY':
            city = reg.group(1)
            return self.get_taxis_by_city(city)
        elif command_type == 'TAXI_INFO':
            city = reg.group(2)
            taxi_id = reg.group(1)
            return self.get_taxi_info(city, taxi_id)
        elif command_type == 'HIRE':
            city = reg.group(2)
            taxi_id = reg.group(1)
            return self.hire_taxi(city, taxi_id)
        elif command_type == 'HIRE_NEAREST':
            city = reg.group(1)
            lat = reg.group(2)
            lon = reg.group(3)
            return self.hire_nearest_taxi(city, lat, lon)
        else:
            return self.answer(self.INVALID_ANSWER)

    def _pprint_taxi(self, taxi_info):
        if isinstance(taxi_info, dict):
            taxi_info = [taxi_info]

        text = ''
        for index, taxi in enumerate(taxi_info):
            text += '{} taxi info:\n'.format(index)
            text += '>>> City: {}\n'.format(taxi['city'])
            text += '>>> Name: {}\n'.format(taxi['name'])
            text += '>>> State: {}\n'.format(taxi['state'])
            text += '>>> Location (lat,lon): {},{}\n'.format(
                taxi['location']['lat'], taxi['location']['lon'])
        return text

    def answer(self, text):
        if isinstance(text, (dict, list)):
            text = self._pprint_taxi(text)
        return self.BASE_ANSWER.format(text)

    def get_taxis(self):
        """
        Get taxis
        """
        return self.answer(self._service.get_taxis())

    def get_taxis_by_city(self, city):
        """
        Hire taxi

        Args:
            city (str): City to get the taxi information
        """
        taxis = self._service.get_taxis_by_city(city)
        if not taxis:
            return self.answer(self.TAXIS_NOT_AVAILABLE_ANSWER.format(city))
        return self.answer(taxis)

    def get_taxi_info(self, city, taxi_id):
        """
        Hire taxi

        Args:
            city (str): City to get the taxi information
            taxi_id (str): Taxi name
        """
        try:
            taxi_info = self._service.get_taxi_info(city, taxi_id)
            return self.answer(taxi_info)
        except taxi_service.TaxiNotExistError:
            return self.answer(self.TAXI_INFO_NOT_AVAILABLE_ANSWER.format(taxi_id))

    def hire_taxi(self, city, taxi_id):
        """
        Hire taxi

        Args:
            city (str): City to get the taxi information
            taxi_id (str): Taxi name
        """
        try:
            taxi_info = self._service.hire_taxi(city, taxi_id)
            return self.answer(taxi_info)
        except taxi_service.NotAvailableTaxiError:
            return self.answer(self.TAXI_INFO_NOT_AVAILABLE_ANSWER.format(taxi_id))

    def hire_nearest_taxi(self, city, lat, lon):
        """
        Hire nearest available taxi providing some coordinates
        """
        try:
            taxi_info = self._service.hire_nearest_taxi(city, float(lat), float(lon))
            return self.answer(taxi_info)
        except TypeError:
            return self.answer('Provided lat and lon data are not valids! Must be float type')
        except taxi_service.NotAvailableTaxiError:
            return self.answer(self.TAXIS_NOT_AVAILABLE_ANSWER.format(city))
