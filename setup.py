"""
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2018-09-02
"""

import os
import sys

from setuptools import find_packages
try:
    from setuptools import setup
    from setuptools import Extension
except ImportError:
    from distutils.core import setup
    from distutils.extension import Extension

def read(fname):
    '''
    Utility function to read the README file. README file is used to create
    the long description.
    '''

    return open(os.path.join(os.path.dirname(__file__), fname)).read()

import taxi_bot

NAME = "taxi-bot"
VERSION = taxi_bot.__version__
DESCRIPTION = "Taxi bot project"

AUTHOR = "Xabier Arbulu"
AUTHOR_EMAIL = "arbulu89@gmail.com"

LICENSE = ''

CLASSIFIERS = [
    "Development Status :: 4 - Beta",
    "Environment :: Console",
    "Intended Audience :: Developers",
    "License :: Other/Proprietary License",
    "Natural Language :: English",
    "Operating System :: Unix",
    "Operating System :: Microsoft :: Windows",
    "Programming Language :: Python :: 2.7",
    "Programming Language :: Python :: 2 :: Only",
]

SCRIPTS = []
DEPENDENCIES = []
DEPENDENCIES = read('requirements.txt').split()

PACKAGE_DATA = {}
DATA_FILES = []

REQUIRED_VERSION = '2.7'

SETUP_PARAMS = dict(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    long_description=read('README.md'),
    packages=find_packages(),
    package_data=PACKAGE_DATA,
    license=LICENSE,
    scripts=SCRIPTS,
    data_files=DATA_FILES,
    install_requires=DEPENDENCIES,
    classifiers=CLASSIFIERS,
)


def main():
    '''
    Setup.py main.
    '''

    if sys.version_info >= (3, 0):
        print '%s %s does not support Python 3 yet'%(NAME, VERSION)
        sys.exit(-1)

    if sys.version < REQUIRED_VERSION:
        print '%s %s requires Python %s or later'%(NAME, VERSION, REQUIRED_VERSION)
        sys.exit(-1)

    setup(**SETUP_PARAMS)

if __name__ == "__main__":
    main()
