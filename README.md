# Taxi bot
Taxi bot project implements different bots to interact with cabify taxi hiring service. This service exposes the taxis in the system and gives the option to hire them.

## Project structure
The project has been structured in the next main two folders:

- taxi_bot. This folder contains all the code related to the taxi hiring service. It follows the repository/service design pattern.

- tests. This folder contains the created tests for this service. The tests are divided in: Unit tests and functional/acceptance tests.

### Disclaimer
- Due lack of time, only the service layer has been unit tested as i consider that is the most important piece of code
- The functional/acceptance tests are used to test the server side code, and the offered endpoints. I have done this because the client side part (the bot) it's mainly used for testing purposes and it's not so critical compared with the server code.

## Issues
After creating and executing the acceptance test suite, some issues have been reported. We can find them in:
[Issues](https://bitbucket.org/arbulu89/taxi-bot/issues?status=new&status=open)

## How to run the app
### Basic shell app
In order to run the basic shell we need to follow the next steps (python and pip must be already installed):
1. In order to keep a clean environment we will create a virtual environment (this step is optional)
```
virtualenv taxi_bot_env
source taxi_bot_env/bin/activate
```

2. Install the project code
```
cd taxi_bot
pip install -r requirements.txt
pip install .
```

3. Start the bot (use --help option to see the arguments)
```
python app_basic.py
```

### Slack bot
In order to run the slack bot we need to have a workspac, a bot and a channel already created.
1. In order to keep a clean environment we will create a virtual environment (this step is optional)
```
virtualenv taxi_bot_env
source taxi_bot_env/bin/activate
```

2. Install the project code.
```
cd taxi_bot
pip install -r requirements.txt
pip install .
```

*INFO*: By default, websocket-client installed by pip does not work. We need to downgrade this library to previous versions.
```
pip install websocket-client==0.47.0
```

3. Start the bot (use --help option to see the arguments). First create a oath in slack to the bot.
```
export SLACK_BOT_TOKEN={slack_bot_token}
python app_slack.py
```

4. Go the your workspace, to the created channel where the bot is as other user and talk with him.
Run help command to check the available option

## How to run the test
### Unit tests

```
pip install pytest mock pytest_cov
cd taxi-bot
py.test --cov=taxi_bot --cov-report html .
```

### Acceptance tests
Acceptance tests are done using [behave](https://github.com/behave/behave) framework

```
pip install -r tests/functional_tests/requirements.txt
cd taxi-bot/tests/functional_tests
behave service.feature
```

## Pending to do...
1. Create the bot that interacts with slack
2. Unit test all the layers
3. Acceptance tests might be added to test the slack bot (anyway, I don't really consider this option really interesting, I don't usually like to test the user interfaces due the effort/value relation).
4. Improve documentation
